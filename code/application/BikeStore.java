package application;
import vehicles.*;

public class BikeStore {
    public static void main(String[] args) {

        // Object Initialization
        Bicycle[] bic = new Bicycle[4];
        bic[0] = new Bicycle("Johnson's Bikes",6,30.5);
        bic[1] = new Bicycle("The Bikers",5,45.2);
        bic[2] = new Bicycle("Biking101",4,54.6);
        bic[3] = new Bicycle("WeLoveBiking",7,37.3);
    
        // Printing
        for (int x = 0; x < bic.length; x++) {
            System.out.println(bic[x]);
        }
    }
}
